/*
 ************************************************************************************************

 *
 * Filename		 : lcd_i2c.c
 * Programmer(s) :
 * Created on	 : 23 abr. 2021
 * Description 	 : ...
 ************************************************************************************************
 */

/*
 ************************************************************************************************
 *								 		INCLUDE FILES
 ************************************************************************************************
 */

#include <lcd_i2c.h>

/*
 ************************************************************************************************
 *								  PRIVATE CONSTANTS & MACROS
 ************************************************************************************************
 */

/* Lcd commands */
#define LCD_CLEARDISPLAY 		0x01
#define LCD_RETURNHOME 			0x02
#define LCD_ENTRYMODESET 		0x04
#define LCD_DISPLAYCONTROL 		0x08
#define LCD_CURSORSHIFT 		0x10
#define LCD_FUNCTIONSET 		0x20
#define LCD_SETCGRAMADDR 		0x40
#define LCD_SETDDRAMADDR 		0x80

/* Entry mode bitfields */
#define LCD_ENTRY_SH 			0x01
#define LCD_ENTRY_ID 			0x02

/* Display control */
#define LCD_DISPLAY_B 			0x01
#define LCD_DISPLAY_C 			0x02
#define LCD_DISPLAY_D 			0x04

/* Shift control */
#define LCD_SHIFT_RL 			0x04
#define LCD_SHIFT_SC 			0x08

/* Function set control */
#define LCD_FUNCTION_F 			0x04
#define LCD_FUNCTION_N 			0x08
#define LCD_FUNCTION_DL 		0x10

/* I2C control bits */
#define LCD_RS 					(1 << 0)
#define LCD_RW 					(1 << 1)
#define LCD_EN 					(1 << 2)
#define LCD_BK_LIGHT 			(1 << 3)

/* Possible slave addres  */
#define LCD_I2C_SLAVE_ADDRESS_0 0x4E
#define LCD_I2C_SLAVE_ADDRESS_1 0x7E

/*
************************************************************************************************
*									  PRIVATE VARIABLES
************************************************************************************************
*/

static I2C_HandleTypeDef *LCD_I2CHandle;
static uint8_t LCD_I2C_SLAVE_ADDRESS = 0;

/*
 ************************************************************************************************
 *								  PRIVATE FUNCTION PROTOTYPES
 ************************************************************************************************
 */

static void LCD_I2C_sendCommand	(uint8_t command);
static void LCD_I2C_sendData	(uint8_t data);

/*
 ************************************************************************************************
 *								DEFINITION OF PRIVATE FUNCTIONS
 ************************************************************************************************
 */


static void LCD_I2C_sendCommand(uint8_t command)
{
	const uint8_t command_0_3 = (0xF0 & (command << 4));
	const uint8_t command_4_7 = (0xF0 & command);
	uint8_t i2cData[4] =

	{
			command_4_7 | LCD_EN | LCD_BK_LIGHT,
			command_4_7 | LCD_BK_LIGHT,
			command_0_3 | LCD_EN | LCD_BK_LIGHT,
			command_0_3 | LCD_BK_LIGHT,
	};
	HAL_I2C_Master_Transmit(LCD_I2CHandle, LCD_I2C_SLAVE_ADDRESS, i2cData, 4, 200);
}

static void LCD_I2C_sendData(uint8_t data)
{
	const uint8_t data_0_3 = (0xF0 & (data << 4));
	const uint8_t data_4_7 = (0xF0 &  data		);
	uint8_t i2cData[4] =
	{
			data_4_7 | LCD_EN | LCD_BK_LIGHT | LCD_RS,
			data_4_7 | LCD_BK_LIGHT | LCD_RS,
			data_0_3 | LCD_EN | LCD_BK_LIGHT | LCD_RS,
			data_0_3 | LCD_BK_LIGHT | LCD_RS,
	};

	HAL_I2C_Master_Transmit(LCD_I2CHandle, LCD_I2C_SLAVE_ADDRESS, i2cData, 4, 200);
}


/*
************************************************************************************************
*								DEFINITION OF PUBLIC FUNCTIONS
************************************************************************************************
*/



/**
 * @brief Initialise LCD16x2
 * @param[in] *pI2cHandle - pointer to HAL I2C handle
 */

bool LCD_I2C_Init(I2C_HandleTypeDef *pI2cHandle)
{
	OSTimeDlyHMSM(0u, 0u, 0u, 50u);

	/* Define el handler del I2C */
	LCD_I2CHandle = pI2cHandle;

	/* Define el addres del slave */
	if (HAL_I2C_IsDeviceReady(LCD_I2CHandle, LCD_I2C_SLAVE_ADDRESS_0, 5, 500) != HAL_OK)
		if (HAL_I2C_IsDeviceReady(LCD_I2CHandle, LCD_I2C_SLAVE_ADDRESS_1, 5, 500) != HAL_OK)
			return false;
		else
			LCD_I2C_SLAVE_ADDRESS = LCD_I2C_SLAVE_ADDRESS_1;
	else
		LCD_I2C_SLAVE_ADDRESS = LCD_I2C_SLAVE_ADDRESS_0;

	/* Initialise LCD for 4-bit operation */
	OSTimeDlyHMSM(0u, 0u, 0u, 45u);
	LCD_I2C_sendCommand(0x30);
	OSTimeDlyHMSM(0u, 0u, 0u, 45u);
	LCD_I2C_sendCommand(0x30);
	OSTimeDlyHMSM(0u, 0u, 0u, 1u);
	LCD_I2C_sendCommand(0x30);
	OSTimeDlyHMSM(0u, 0u, 0u, 8u);
	LCD_I2C_sendCommand(0x20);
	OSTimeDlyHMSM(0u, 0u, 0u, 8u);

	LCD_I2C_sendCommand(LCD_FUNCTIONSET | LCD_FUNCTION_N);
	OSTimeDlyHMSM(0u, 0u, 0u, 1u);
	LCD_I2C_sendCommand(LCD_DISPLAYCONTROL);
	OSTimeDlyHMSM(0u, 0u, 0u, 1u);
	LCD_I2C_sendCommand(LCD_CLEARDISPLAY);
	OSTimeDlyHMSM(0u, 0u, 0u, 3u);
	LCD_I2C_sendCommand(0x04 | LCD_ENTRY_ID);
	OSTimeDlyHMSM(0u, 0u, 0u, 1u);
	LCD_I2C_sendCommand(LCD_DISPLAYCONTROL | LCD_DISPLAY_D);
	OSTimeDlyHMSM(0u, 0u, 0u, 3u);

	return true;
}

/**
 * @brief Set cursor position
 * @param[in] row - 0 or 1 for line1 or line2
 * @param[in] col - 0 - 15 (16 columns LCD)
 */

void LCD_I2C_SetCursor(uint8_t row, uint8_t col)
{
	uint8_t maskData;
	maskData = (col)&0x0F;
	if (row == 0)
	{
		maskData |= (0x80);
		LCD_I2C_sendCommand(maskData);
	}
	else
	{
		maskData |= (0xc0);
		LCD_I2C_sendCommand(maskData);
	}
}

/**
 * @brief Move to beginning of 1st line
 */
void LCD_I2C_GoFirstLine(void)
{
	LCD_I2C_SetCursor(0, 0);
}
/**
 * @brief Move to beginning of 2nd line
 */
void LCD_I2C_GoSecondLine(void)
{
	LCD_I2C_SetCursor(1, 0);
}

/**
 * @brief Select LCD Number of lines mode
 */
void LCD_I2C_TwoLines(void)
{
	LCD_I2C_sendCommand(LCD_FUNCTIONSET | LCD_FUNCTION_N);
}
void LCD_I2C_OneLine(void)
{
	LCD_I2C_sendCommand(LCD_FUNCTIONSET);
}

/**
 * @brief Cursor ON/OFF
 */
void LCD_I2C_CursorShow(bool state)
{
	if (state)
		LCD_I2C_sendCommand(LCD_DISPLAYCONTROL | LCD_DISPLAY_B | LCD_DISPLAY_C | LCD_DISPLAY_D);
	else
		LCD_I2C_sendCommand(LCD_DISPLAYCONTROL | LCD_DISPLAY_D);
}

/**
 * @brief Display clear
 */
void LCD_I2C_Clear(void)
{
	LCD_I2C_sendCommand(LCD_CLEARDISPLAY);
	OSTimeDlyHMSM(0u, 0u, 0u, 3u);
}

/**
 * @brief Display ON/OFF, to hide all characters, but not clear
 */
void LCD_I2C_Display(bool state)
{
	if (state)
		LCD_I2C_sendCommand(LCD_DISPLAYCONTROL | LCD_DISPLAY_B | LCD_DISPLAY_C | LCD_DISPLAY_D);
	else
		LCD_I2C_sendCommand(LCD_DISPLAYCONTROL | LCD_DISPLAY_B | LCD_DISPLAY_C);
}

/**
 * @brief Shift content to right
 */
void LCD_I2C_ShiftRight(uint8_t offset)
{
	for (uint8_t i = 0; i < offset; i++)
		LCD_I2C_sendCommand(0x1c);
}

/**
 * @brief Shift content to left
 */
void LCD_I2C_ShiftLeft(uint8_t offset)
{
	for (uint8_t i = 0; i < offset; i++)
		LCD_I2C_sendCommand(0x18);
}

/**
 * @brief Print to display
 */
void LCD_I2C_Printf(const char *str, ...)
{
	char stringArray[20];
	va_list args;
	va_start(args, str);
	vsprintf(stringArray, str, args);
	va_end(args);

	for (uint8_t i = 0; i < strlen(stringArray) && i < 32; i++)
		LCD_I2C_sendData((uint8_t)stringArray[i]);
}

void LCD_I2C_PutMeasure          (float x, char *p){
	// Le paso el valor flotante y el char del titulo de medida para que ponga en display
	char str[10] = " ";
	float_to_char(x, &str[0],5); //convierto pH en str para poner en display
	//LCD_I2C_SetCursor(0, 0);
	LCD_I2C_Printf("%s=%s ",p,str);
}

char * float_to_char(float x, char *p, int CHAR_BUFF_SIZE) {
    char *s = p + CHAR_BUFF_SIZE; // go to end of buffer
    uint16_t decimals;  // variable to store the decimals
    int units;  // variable to store the units (part to left of decimal place)
    if (x < 0) { // take care of negative numbers
        decimals = (int)(x * -100) % 100; // make 1000 for 3 decimals etc.
        units = (int)(-1 * x);
    } else { // positive numbers
        decimals = (int)(x * 100) % 100;
        units = (int)x;
    }

    *--s = (decimals % 10) + '0';
    decimals /= 10; // repeat for as many decimal places as you need
    *--s = (decimals % 10) + '0';
    *--s = '.';

    while (units > 0) {
        *--s = (units % 10) + '0';
        units /= 10;
    }
    if (x < 0) *--s = '-'; // unary minus sign for negative numbers
    return s;
}

