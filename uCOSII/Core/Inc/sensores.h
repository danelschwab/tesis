/*
 * sensores.h
 *
 *  Created on: Sep 7, 2021
 *      Author: Danel
 */

#ifndef SENSORES_H_
#define SENSORES_H_

#include "stm32f1xx_hal.h"
#include "main.h"
#include <ucos_ii.h>

#define TRIG_PIN GPIO_PIN_9
#define TRIG_PORT GPIOA
#define DS18B20_PIN GPIO_PIN_9
#define DS18B20_PORT GPIOB




float lectura_pH();
float lectura_temp();
float lectura_nivel_recip1();
float lectura_nivel_recip2();
void delay (uint16_t);
void Set_Pin_Output (GPIO_TypeDef *, uint16_t);
void Set_Pin_Input (GPIO_TypeDef *, uint16_t);
uint8_t DS18B20_Start ();
void DS18B20_Write (uint8_t);
uint8_t DS18B20_Read ();
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *);
void HCSR04_Read_1 (void);
void HCSR04_Read_2 (void);

#endif /* SENSORES_H_ */
