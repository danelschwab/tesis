/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include "stdio.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "lib_str.h"
#include "stdarg.h"
#include "lcd_i2c.h"
#include "sensores.h"
#include <ucos_ii.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 16u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;




/* USER CODE BEGIN PV */
static void StartupTask (void *p_arg);
static void Time 		(void *p_arg);
static void Teclas 		(void *p_arg);
static void Display 	(void *p_arg);
static void Led 		(void *p_arg);

static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK TimeStk[APP_CFG_Time_STK_SIZE];
static OS_STK TeclasStk[APP_CFG_Teclas_STK_SIZE];
static OS_STK DisplayStk[APP_CFG_Display_STK_SIZE];
static OS_STK LedStk[APP_CFG_Led_STK_SIZE];

static void App_TaskCreate (void);


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//uint32_t pH = 0;
//uint8_t nivel = 0; //Si esta en 1 resetea la cuenta
float pH =0;
int nivel_pileta = 0;
float temperatura=0;
float nivel_pH=0;
float nivel_clarif=0;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
CPU_INT08U os_err;
#endif
CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  CPU_IntDis();
  for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
  {
  /* Set all external intr. to KA interrupt priority boundary */
  CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
  }
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_1);
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_3);
  HAL_TIM_Base_Start(&htim1);

  OSInit();
  OSTaskCreateExt( StartupTask,
                   0,
				   &StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
				   APP_CFG_STARTUP_TASK_PRIO,
				   APP_CFG_STARTUP_TASK_PRIO,
				   &StartupTaskStk[0],
				   APP_CFG_STARTUP_TASK_STK_SIZE,
				   0,
				   (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
  #if (OS_TASK_NAME_EN > 0u)
   OSTaskNameSet( APP_CFG_STARTUP_TASK_PRIO,
    	  	      (INT8U *)"Startup task",
				  &os_err);
  #endif
  OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 72-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xFFFF-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_IC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(pH_encendido_GPIO_Port, pH_encendido_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, sens_temp_Pin|dac_on_Pin|dac_2_Pin|dac_1_Pin
                          |bomb_220_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, tecl_o1_Pin|tecl_o2_Pin|tecl_o3_Pin|tecl_o4_Pin
                          |puente_H_1_Pin|puente_H_2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC13 pH_encendido_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_13|pH_encendido_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : sens_220_Pin */
  GPIO_InitStruct.Pin = sens_220_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(sens_220_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : sens_temp_Pin dac_on_Pin dac_2_Pin dac_1_Pin
                           bomb_220_Pin */
  GPIO_InitStruct.Pin = sens_temp_Pin|dac_on_Pin|dac_2_Pin|dac_1_Pin
                          |bomb_220_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : sens_bat_Pin */
  GPIO_InitStruct.Pin = sens_bat_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(sens_bat_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : tecl_o1_Pin tecl_o2_Pin tecl_o3_Pin tecl_o4_Pin
                           puente_H_1_Pin puente_H_2_Pin */
  GPIO_InitStruct.Pin = tecl_o1_Pin|tecl_o2_Pin|tecl_o3_Pin|tecl_o4_Pin
                          |puente_H_1_Pin|puente_H_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : tecl_i1_Pin tecl_i2_Pin tecl_i3_Pin tecl_i4_Pin */
  GPIO_InitStruct.Pin = tecl_i1_Pin|tecl_i2_Pin|tecl_i3_Pin|tecl_i4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : trig_1_Pin */
  GPIO_InitStruct.Pin = trig_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(trig_1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : nivel_Pin */
  GPIO_InitStruct.Pin = nivel_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(nivel_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/*
*************************************************************************
* STM32Cube HAL FUNCTIONS
*************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
 return (HAL_OK);
}

uint32_t HAL_GetTick(void)
{
 CPU_INT32U os_tick_ctr;

 #if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
 #else
  os_tick_ctr = OSTimeGet();
 #endif

 return os_tick_ctr;
}

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void StartupTask (void *p_arg)
{
 CPU_INT32U cpu_clk;
 (void)p_arg;
 cpu_clk = HAL_RCC_GetHCLKFreq();
 /* Initialize and enable System Tick timer */
 OS_CPU_SysTickInitFreq(cpu_clk);

 #if (OS_TASK_STAT_EN > 0)
  OSStatInit();                                               /* Determine CPU capacity.                              */
 #endif

 LCD_I2C_Init(&hi2c1);
// App_EventCreate();                                          /* Create application events.                           */
 App_TaskCreate();                                           /* Create application tasks.                            */
 OSTaskDel(2u);
 /*while (DEF_TRUE){
   HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
   UsbPrintf("uCOS-II Running...\n");
   OSTimeDlyHMSM(0u, 0u, 1u, 0u);
  }*/
}

/*
*********************************************************************************************************
*                                            App_TaskCreate()
*
* Description : Create the application tasks.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : App_TaskStart().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  App_TaskCreate (void)
{
    CPU_INT08U  os_err;

    //Tarea 1 - Time

    os_err = OSTaskCreateExt((void (*)(void *)) Time,
                             (void          * ) 0,
                             (OS_STK        * )&TimeStk[APP_CFG_Time_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_Time_PRIO,
                             (INT16U          ) APP_CFG_Time_PRIO,
                             (OS_STK        * )&TimeStk[0],
                             (INT32U          ) APP_CFG_Time_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
     OSTaskNameSet(APP_CFG_Time_PRIO, (INT8U *)"Task 1", &os_err);
    #endif
     //tarea 2

     os_err = OSTaskCreateExt((void (*)(void *)) Teclas,
                              (void          * ) 0,
                              (OS_STK        * )&TeclasStk[APP_CFG_Time_STK_SIZE - 1],
                              (INT8U           ) APP_CFG_Teclas_PRIO,
                              (INT16U          ) APP_CFG_Teclas_PRIO,
                              (OS_STK        * )&TeclasStk[0],
                              (INT32U          ) APP_CFG_Teclas_STK_SIZE,
                              (void          * ) 0,
                              (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

     #if (OS_TASK_NAME_EN > 0u)
      OSTaskNameSet(APP_CFG_Teclas_PRIO, (INT8U *)"Task 2", &os_err);
     #endif
	  //tarea 3

	  os_err = OSTaskCreateExt((void (*)(void *)) Display,
	 						  (void          * ) 0,
	 						  (OS_STK        * )&DisplayStk[APP_CFG_Display_STK_SIZE - 1],
	 						  (INT8U           ) APP_CFG_Display_PRIO,
	 						  (INT16U          ) APP_CFG_Display_PRIO,
	 						  (OS_STK        * )&DisplayStk[0],
	 						  (INT32U          ) APP_CFG_Display_STK_SIZE,
	 						  (void          * ) 0,
	 						  (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	  #if (OS_TASK_NAME_EN > 0u)
	   OSTaskNameSet(APP_CFG_Display_PRIO, (INT8U *)"Task 3", &os_err);
	  #endif
	   //tarea 4

	   os_err = OSTaskCreateExt((void (*)(void *)) Led,
	  						  (void          * ) 0,
	  						  (OS_STK        * )&LedStk[APP_CFG_Led_STK_SIZE - 1],
	  						  (INT8U           ) APP_CFG_Led_PRIO,
	  						  (INT16U          ) APP_CFG_Led_PRIO,
	  						  (OS_STK        * )&LedStk[0],
	  						  (INT32U          ) APP_CFG_Led_STK_SIZE,
	  						  (void          * ) 0,
	  						  (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	   #if (OS_TASK_NAME_EN > 0u)
	    OSTaskNameSet(APP_CFG_Led_PRIO, (INT8U *)"Task 4", &os_err);
	   #endif


}

/*
**************************************************************************************************************************
*                                               Tareas
*
* Description : Incrementa el tiempo de a 100ms.
* Argument(s) : none
* Return(s)   : none.
* Caller(s)   :
* Note(s)     : none.
**************************************************************************************************************************
*/

static void Time (void *p_arg){

	(void)p_arg;

	while (DEF_TRUE){
		//UsbPrintf("Tarea tiempo ... \n\r");


		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

static void Teclas (void *p_arg){

	(void)p_arg;
	//float pH;
	char str[10] = " ";

	while (DEF_TRUE){

		//Obtengo nivel de la pileta
		nivel_pileta = HAL_GPIO_ReadPin(nivel_GPIO_Port, nivel_Pin) ;

		//Obtengo pH
		pH = lectura_pH();
		//HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		float_to_char(pH, &str[0],5); //convierto pH en str para poner en display
		LCD_I2C_SetCursor(0, 0);
		LCD_I2C_Printf("PH =%s ",str);
		OSTimeDlyHMSM(0u, 0u, 1u, 0u);
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		//LCD_I2C_Clear();
		temperatura = lectura_temp();
		//float_to_char(temperatura, &str[0],5); //convierto pH en str para poner en display
		//LCD_I2C_SetCursor(0, 0);
		//LCD_I2C_Printf("Temp =%s ",str);

		//OS_EXIT_CRITICAL();
		OSTimeDlyHMSM(0u, 0u, 1u, 0u);

		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);

		//LCD_I2C_Clear();
		nivel_pH = lectura_nivel_recip1();
		//float_to_char(nivel_pH, &str[0],5); //convierto pH en str para poner en display
		//LCD_I2C_SetCursor(0, 0);
		//LCD_I2C_Printf("pH- =%s % ",str);
		OSTimeDlyHMSM(0u, 0u, 1u, 0u);

		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		//LCD_I2C_Clear();
		nivel_clarif = lectura_nivel_recip2();
		//float_to_char(nivel_pH, &str[0],5); //convierto pH en str para poner en display
		//LCD_I2C_SetCursor(0, 0);
		//LCD_I2C_Printf("pH- =%s % ",str);
		OSTimeDlyHMSM(0u, 0u, 1u, 0u);


		LCD_I2C_Clear();
		//OSTimeDlyHMSM(0u, 0u, 0u, 500u);
		//HAL_GPIO_TogglePin(sens_temp_GPIO_Port, sens_temp_Pin)



	}
}

static void Display (void *p_arg){

	(void)p_arg;

	while (1){
		OSTimeDlyHMSM(0u, 0u, 0u, 30u);
	}
}

static void Led (void *p_arg){

	(void)p_arg;

	while (DEF_TRUE){
		//UsbPrintf("Tarea LED ... \n\r");
		/*if (nivel == 1){
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		}*/



		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}




/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
