/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include "stdio.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "lib_str.h"
#include "stdarg.h"
#include "lcd_i2c.h"
#include "sensores.h"
#include <ucos_ii.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 18u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */
static void StartupTask 	(void *p_arg);
static void Tiempo     		(void *p_arg);
static void Teclas      	(void *p_arg);
static void Display     	(void *p_arg);
static void Sensores_1      (void *p_arg);
static void Sensores_2      (void *p_arg);
static void Led     		(void *p_arg);
static void Bomba_filtrado  (void *p_arg);
static void Bomba_dosif_1   (void *p_arg);
static void Bomba_dosif_2   (void *p_arg);
static void Ionizacion      (void *p_arg);
static void Menu     		(void *p_arg);
static void Manejo_salidas  (void *p_arg);
static void medicion_pH     (void *p_arg);

static void App_TaskCreate (void);


static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK TiempoStk[APP_CFG_Tiempo_STK_SIZE];
static OS_STK TeclasStk[APP_CFG_Teclas_STK_SIZE];
static OS_STK DisplayStk[APP_CFG_Display_STK_SIZE];
static OS_STK Sensores_1Stk[APP_CFG_Sensores_1_STK_SIZE];
static OS_STK Sensores_2Stk[APP_CFG_Sensores_2_STK_SIZE];
static OS_STK LedStk[APP_CFG_Led_STK_SIZE];
static OS_STK Bomba_filtradoStk[APP_CFG_Bomba_filtrado_STK_SIZE];
static OS_STK Bomba_dosif_1Stk[APP_CFG_Bomba_dosif_1_STK_SIZE];
static OS_STK Bomba_dosif_2Stk[APP_CFG_Bomba_dosif_2_STK_SIZE];
static OS_STK IonizacionStk[APP_CFG_Ionizacion_STK_SIZE];
static OS_STK MenuStk[APP_CFG_Menu_STK_SIZE];
static OS_STK Manejo_salidasStk[APP_CFG_Manejo_salidas_STK_SIZE];
static OS_STK medicion_pHStk[APP_CFG_medicion_pH_STK_SIZE];



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

uint32_t pH_adc = 0;
float pH =0;
int nivel_pileta = 0;
float temperatura=0;
float nivel_pH=0;
float nivel_clarif=0;


CPU_INT08U seg=0;
CPU_INT08U min=0;
CPU_INT08U hora=0;

CPU_BOOLEAN pres_tecla=0;

CPU_INT08U  tecla_pres=0;


CPU_INT32U  litros_pileta=0;
CPU_INT08U  indice=0;
CPU_INT08U  menu = 0;
int litros[8];
int hora_button[2];
int minutos_button[2];

CPU_INT08U hora_activa = 0;
CPU_INT08U minutos_activa = 0;


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
CPU_INT08U os_err;
#endif
CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */

HAL_Init();

  /* USER CODE BEGIN Init */
  CPU_IntDis();
  for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
  {
  /* Set all external intr. to KA interrupt priority boundary */
  CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
  }
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_1);
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_3);
  HAL_TIM_Base_Start(&htim1);

  OSInit();
  OSTaskCreateExt( StartupTask,
                   0,
				   &StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
				   APP_CFG_STARTUP_TASK_PRIO,
				   APP_CFG_STARTUP_TASK_PRIO,
				   &StartupTaskStk[0],
				   APP_CFG_STARTUP_TASK_STK_SIZE,
				   0,
				   (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
  #if (OS_TASK_NAME_EN > 0u)
   OSTaskNameSet( APP_CFG_STARTUP_TASK_PRIO,
    	  	      (INT8U *)"Startup task",
				  &os_err);
  #endif
  OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 72-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xFFFF-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_IC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, led_Pin|pH_encendido_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(e_relay_GPIO_Port, e_relay_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, puente_H_1_Pin|puente_H_2_Pin|dac_on_Pin|dac_1_Pin
                          |dac_2_Pin|GPIO_PIN_9|dosif_pH_Pin|dosif_clarif_Pin
                          |bomb_220_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, tecl_o4_Pin|tecl_o3_Pin|tecl_o2_Pin|tecl_o1_Pin
                          |sens_temp_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : led_Pin e_relay_Pin pH_encendido_Pin */
  GPIO_InitStruct.Pin = led_Pin|e_relay_Pin|pH_encendido_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : puente_H_1_Pin puente_H_2_Pin dac_on_Pin dac_1_Pin
                           dac_2_Pin PA9 dosif_pH_Pin dosif_clarif_Pin
                           bomb_220_Pin */
  GPIO_InitStruct.Pin = puente_H_1_Pin|puente_H_2_Pin|dac_on_Pin|dac_1_Pin
                          |dac_2_Pin|GPIO_PIN_9|dosif_pH_Pin|dosif_clarif_Pin
                          |bomb_220_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : sens_bat_Pin */
  GPIO_InitStruct.Pin = sens_bat_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(sens_bat_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : tecl_o4_Pin tecl_o3_Pin tecl_o2_Pin tecl_o1_Pin
                           sens_temp_Pin */
  GPIO_InitStruct.Pin = tecl_o4_Pin|tecl_o3_Pin|tecl_o2_Pin|tecl_o1_Pin
                          |sens_temp_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : tecl_i1_Pin tecl_i2_Pin tecl_i3_Pin tecl_i4_Pin
                           nivel_Pin */
  GPIO_InitStruct.Pin = tecl_i1_Pin|tecl_i2_Pin|tecl_i3_Pin|tecl_i4_Pin
                          |nivel_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : sens_220_Pin */
  GPIO_InitStruct.Pin = sens_220_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(sens_220_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/*
*************************************************************************
* STM32Cube HAL FUNCTIONS
*************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
 return (HAL_OK);
}

uint32_t HAL_GetTick(void)
{
 CPU_INT32U os_tick_ctr;

 #if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
 #else
  os_tick_ctr = OSTimeGet();
 #endif

 return os_tick_ctr;
}

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void StartupTask (void *p_arg)
{
 CPU_INT32U cpu_clk;
 (void)p_arg;
 cpu_clk = HAL_RCC_GetHCLKFreq();
 /* Initialize and enable System Tick timer */
 OS_CPU_SysTickInitFreq(cpu_clk);

 #if (OS_TASK_STAT_EN > 0)
  OSStatInit();                                               /* Determine CPU capacity.                              */
 #endif

 LCD_I2C_Init(&hi2c1);
// App_EventCreate();                                          /* Create application events.                           */
 App_TaskCreate();                                           /* Create application tasks.                            */
 OSTaskDel(2u);
 /*while (DEF_TRUE){
   HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
   UsbPrintf("uCOS-II Running...\n");
   OSTimeDlyHMSM(0u, 0u, 1u, 0u);
  }*/
}

/*
*********************************************************************************************************
*                                            App_TaskCreate()
*
* Description : Create the application tasks.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : App_TaskStart().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  App_TaskCreate (void)
{
    CPU_INT08U  os_err;

    os_err = OSTaskCreateExt((void (*)(void *)) Tiempo,
    			(void	       *) 0,
    			(OS_STK        *)&TiempoStk[APP_CFG_Tiempo_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Tiempo_PRIO,
    			(INT16U         ) APP_CFG_Tiempo_PRIO,
    			(OS_STK        *)&TiempoStk[0],
    			(INT32U         ) APP_CFG_Tiempo_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Tiempo_PRIO,(INT8U *)"Task 1" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Teclas,
    			(void	       *) 0,
    			(OS_STK        *)&TeclasStk[APP_CFG_Teclas_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Teclas_PRIO,
    			(INT16U         ) APP_CFG_Teclas_PRIO,
    			(OS_STK        *)&TeclasStk[0],
    			(INT32U         ) APP_CFG_Teclas_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Teclas_PRIO,(INT8U *)"Task 2" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Display,
    			(void	       *) 0,
    			(OS_STK        *)&DisplayStk[APP_CFG_Display_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Display_PRIO,
    			(INT16U         ) APP_CFG_Display_PRIO,
    			(OS_STK        *)&DisplayStk[0],
    			(INT32U         ) APP_CFG_Display_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Display_PRIO,(INT8U *)"Task 3" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Sensores_1,
    			(void	       *) 0,
    			(OS_STK        *)&Sensores_1Stk[APP_CFG_Sensores_1_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Sensores_1_PRIO,
    			(INT16U         ) APP_CFG_Sensores_1_PRIO,
    			(OS_STK        *)&Sensores_1Stk[0],
    			(INT32U         ) APP_CFG_Sensores_1_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Sensores_1_PRIO,(INT8U *)"Task 4" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Sensores_2,
    			(void	       *) 0,
    			(OS_STK        *)&Sensores_2Stk[APP_CFG_Sensores_2_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Sensores_2_PRIO,
    			(INT16U         ) APP_CFG_Sensores_2_PRIO,
    			(OS_STK        *)&Sensores_2Stk[0],
    			(INT32U         ) APP_CFG_Sensores_2_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Sensores_2_PRIO,(INT8U *)"Task 5" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Led,
    			(void	       *) 0,
    			(OS_STK        *)&LedStk[APP_CFG_Led_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Led_PRIO,
    			(INT16U         ) APP_CFG_Led_PRIO,
    			(OS_STK        *)&LedStk[0],
    			(INT32U         ) APP_CFG_Led_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Led_PRIO,(INT8U *)"Task 6" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Bomba_filtrado,
    			(void	       *) 0,
    			(OS_STK        *)&Bomba_filtradoStk[APP_CFG_Bomba_filtrado_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Bomba_filtrado_PRIO,
    			(INT16U         ) APP_CFG_Bomba_filtrado_PRIO,
    			(OS_STK        *)&Bomba_filtradoStk[0],
    			(INT32U         ) APP_CFG_Bomba_filtrado_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Bomba_filtrado_PRIO,(INT8U *)"Task 7" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Bomba_dosif_1,
    			(void	       *) 0,
    			(OS_STK        *)&Bomba_dosif_1Stk[APP_CFG_Bomba_dosif_1_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Bomba_dosif_1_PRIO,
    			(INT16U         ) APP_CFG_Bomba_dosif_1_PRIO,
    			(OS_STK        *)&Bomba_dosif_1Stk[0],
    			(INT32U         ) APP_CFG_Bomba_dosif_1_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Bomba_dosif_1_PRIO,(INT8U *)"Task 8" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Bomba_dosif_2,
    			(void	       *) 0,
    			(OS_STK        *)&Bomba_dosif_2Stk[APP_CFG_Bomba_dosif_2_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Bomba_dosif_2_PRIO,
    			(INT16U         ) APP_CFG_Bomba_dosif_2_PRIO,
    			(OS_STK        *)&Bomba_dosif_2Stk[0],
    			(INT32U         ) APP_CFG_Bomba_dosif_2_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Bomba_dosif_2_PRIO,(INT8U *)"Task 9" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Ionizacion,
    			(void	       *) 0,
    			(OS_STK        *)&IonizacionStk[APP_CFG_Ionizacion_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Ionizacion_PRIO,
    			(INT16U         ) APP_CFG_Ionizacion_PRIO,
    			(OS_STK        *)&IonizacionStk[0],
    			(INT32U         ) APP_CFG_Ionizacion_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Ionizacion_PRIO,(INT8U *)"Task 10" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Menu,
    			(void	       *) 0,
    			(OS_STK        *)&MenuStk[APP_CFG_Menu_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Menu_PRIO,
    			(INT16U         ) APP_CFG_Menu_PRIO,
    			(OS_STK        *)&MenuStk[0],
    			(INT32U         ) APP_CFG_Menu_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Menu_PRIO,(INT8U *)"Task 11" ,&os_err);
    #endif
    os_err = OSTaskCreateExt((void (*)(void *)) Manejo_salidas,
    			(void	       *) 0,
    			(OS_STK        *)&Manejo_salidasStk[APP_CFG_Manejo_salidas_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_Manejo_salidas_PRIO,
    			(INT16U         ) APP_CFG_Manejo_salidas_PRIO,
    			(OS_STK        *)&Manejo_salidasStk[0],
    			(INT32U         ) APP_CFG_Manejo_salidas_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_Manejo_salidas_PRIO,(INT8U *)"Task 12" ,&os_err);
    #endif

    os_err = OSTaskCreateExt((void (*)(void *)) medicion_pH,
    			(void	       *) 0,
    			(OS_STK        *)&medicion_pHStk[APP_CFG_medicion_pH_STK_SIZE - 1],
    			(INT8U          ) APP_CFG_medicion_pH_PRIO,
    			(INT16U         ) APP_CFG_medicion_pH_PRIO,
    			(OS_STK        *)&medicion_pHStk[0],
    			(INT32U         ) APP_CFG_medicion_pH_STK_SIZE,
    			(void          *) 0,
    			(INT16U         ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    #if (OS_TASK_NAME_EN > 0u)
    	OSTaskNameSet(APP_CFG_medicion_pH_PRIO,(INT8U *)"Task 13" ,&os_err);
    #endif




}

/*
**************************************************************************************************************************
*                                               Tareas
*
* Description : Incrementa el tiempo de a 100ms.
* Argument(s) : none
* Return(s)   : none.
* Caller(s)   :
* Note(s)     : none.
**************************************************************************************************************************
*/


static void Tiempo (void *p_arg){

(void)p_arg;
while (DEF_TRUE){

	if (seg==60){
		min=min+1;
		seg=0;
		if (min==60){
			hora = hora +1;
			min = 0;

			if (hora == 24){
				hora = 0;
			}
		}
	}

	OSTimeDlyHMSM(0u, 0u, 1u, 0u);

	seg=seg+1;


	}
}

static void Teclas (void *p_arg){

(void)p_arg;
int i;

while (DEF_TRUE){


	//Pongo un uno en la primer fila y leo las cuatro columnas
	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 1);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);
	i=0;

	if (HAL_GPIO_ReadPin(tecl_i1_GPIO_Port, tecl_i1_Pin)==1){
		pres_tecla=1;
		tecla_pres=1;
	}
	else if (HAL_GPIO_ReadPin(tecl_i2_GPIO_Port, tecl_i2_Pin)==1){
		pres_tecla=1;
		tecla_pres=2;
	}
	else if (HAL_GPIO_ReadPin(tecl_i3_GPIO_Port, tecl_i3_Pin)==1){
		pres_tecla=1;
		tecla_pres=3;
	}
	else if (HAL_GPIO_ReadPin(tecl_i4_GPIO_Port, tecl_i4_Pin)==1){
		pres_tecla=1;
		tecla_pres=10;//A
	}
	else i=i+1;

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);

	OSTimeDlyHMSM(0u, 0u, 0u,70u);

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 1);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);


	//Pongo un uno en la segunda fila y leo las cuatro columnas
	if (HAL_GPIO_ReadPin(tecl_i1_GPIO_Port, tecl_i1_Pin)){
		pres_tecla=1;
		tecla_pres=4;
	}
	else if (HAL_GPIO_ReadPin(tecl_i2_GPIO_Port, tecl_i2_Pin)){
		pres_tecla=1;
		tecla_pres=5;
	}
	else if (HAL_GPIO_ReadPin(tecl_i3_GPIO_Port, tecl_i3_Pin)){
		pres_tecla=1;
		tecla_pres=6;
	}
	else if (HAL_GPIO_ReadPin(tecl_i4_GPIO_Port, tecl_i4_Pin)){
		pres_tecla=1;
		tecla_pres=11;//B
	}
	else i=i+1;

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);

	OSTimeDlyHMSM(0u, 0u, 0u,70u);

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 1);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);

	//Pongo un uno en la tercer fila y leo las cuatro columnas
	if (HAL_GPIO_ReadPin(tecl_i1_GPIO_Port, tecl_i1_Pin)){
		pres_tecla=1;
		tecla_pres=7;
	}
	else if (HAL_GPIO_ReadPin(tecl_i2_GPIO_Port, tecl_i2_Pin)){
		pres_tecla=1;
		tecla_pres=8;
	}
	else if (HAL_GPIO_ReadPin(tecl_i3_GPIO_Port, tecl_i3_Pin)){
		pres_tecla=1;
		tecla_pres=9;
	}
	else if (HAL_GPIO_ReadPin(tecl_i4_GPIO_Port, tecl_i4_Pin)){
		pres_tecla=1;
		tecla_pres=12;//C
	}
	else i=i+1;

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);

	OSTimeDlyHMSM(0u, 0u, 0u,70u);

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 1);

	//Pongo un uno en la cuarta fila y leo las cuatro columnas

	if (HAL_GPIO_ReadPin(tecl_i1_GPIO_Port, tecl_i1_Pin)){
		pres_tecla=1;
		tecla_pres=14; //asterisco
	}
	else if (HAL_GPIO_ReadPin(tecl_i2_GPIO_Port, tecl_i2_Pin)){
		pres_tecla=1;
		tecla_pres=0;
	}
	else if (HAL_GPIO_ReadPin(tecl_i3_GPIO_Port, tecl_i3_Pin)){
		pres_tecla=1;
		tecla_pres=15;//numeral
	}
	else if (HAL_GPIO_ReadPin(tecl_i4_GPIO_Port, tecl_i4_Pin)){
		pres_tecla=1;
		tecla_pres=13; //D
	}
	else i=i+1;



	if (i==4){
		pres_tecla=0;
	}

	HAL_GPIO_WritePin(tecl_o1_GPIO_Port, tecl_o1_Pin, 0);
	HAL_GPIO_WritePin(tecl_o2_GPIO_Port, tecl_o2_Pin, 0);
	HAL_GPIO_WritePin(tecl_o3_GPIO_Port, tecl_o3_Pin, 0);
	HAL_GPIO_WritePin(tecl_o4_GPIO_Port, tecl_o4_Pin, 0);

	OSTimeDlyHMSM(0u, 0u, 0u,70u);
	}
}

static void Display (void *p_arg){

(void)p_arg;
int i=0;

while (DEF_TRUE){


	if (menu == 0){
		if (i == 0){
			LCD_I2C_Printf("BIENVENIDO");
			OSTimeDlyHMSM(0u, 0u, 3u, 0u);
			LCD_I2C_Clear();

			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("INGRESE LITROS");
			LCD_I2C_SetCursor(1, 0);
			LCD_I2C_Printf("DE SU PISCINA");
			OSTimeDlyHMSM(0u, 0u, 3u, 0u);
			LCD_I2C_Clear();
			i = 1;
		}
		else {
		switch (indice){
		case 0:
			LCD_I2C_Printf("XXXXXXXX");
			break;
		case 1:
			LCD_I2C_Printf("%dXXXXXXX",litros[0]);
			break;
		case 2:
			LCD_I2C_Printf("%d%dXXXXXX",litros[0],litros[1]);
			break;
		case 3:
			LCD_I2C_Printf("%d%d%dXXXXX",litros[0],litros[1],litros[2]);
			break;
		case 4:
			LCD_I2C_Printf("%d%d%d%dXXXX",litros[0],litros[1],litros[2],litros[3]);
			break;
		case 5:
			LCD_I2C_Printf("%d%d%d%d%dXXX",litros[0],litros[1],litros[2],litros[3],litros[4]);
			break;
		case 6:
			LCD_I2C_Printf("%d%d%d%d%d%dXX",litros[0],litros[1],litros[2],litros[3],litros[4],litros[5]);
			break;
		case 7:
			LCD_I2C_Printf("%d%d%d%d%d%d%dX",litros[0],litros[1],litros[2],litros[3],litros[4],litros[5],litros[6]);
			break;
		case 8:
			LCD_I2C_Printf("%d%d%d%d%d%d%d%d",litros[0],litros[1],litros[2],litros[3],litros[4],litros[5],litros[6],litros[7]);
			break;
			}
		}
	}

	if(menu ==1 ){
		if (i==1){
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("SU PILETA TIENE");
			LCD_I2C_SetCursor(1, 0);
			LCD_I2C_Printf("%d lt",litros_pileta);
			OSTimeDlyHMSM(0u, 0u, 2u, 0u);
			i = 2;
		}
		else if(i == 2){
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("INGRESE HORA");
			LCD_I2C_SetCursor(1, 0);
			LCD_I2C_Printf("ACTUAL");
			OSTimeDlyHMSM(0u, 0u, 2u, 0u);
			i = 3;

		}

		else if (i==3){
			switch (indice){
			case 0:
				LCD_I2C_Printf("XX:XX");
				break;
			case 1:
				LCD_I2C_Printf("%dX:XX",hora_button[0]);
				break;
			case 2:
				LCD_I2C_Printf("%d%d:XX",hora_button[0],hora_button[1]);
				break;
			case 3:
				LCD_I2C_Printf("%d%d:%dX",hora_button[0],hora_button[1],minutos_button[0]);
				break;
			case 4:
				LCD_I2C_Printf("%d%d:%d%d",hora_button[0],hora_button[1],minutos_button[0],minutos_button[1]);
				break;
					}

				}

		}
	if (menu == 2){
		if (i==3){
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("HORA ACTUAL");
			LCD_I2C_SetCursor(1, 0);
			if (min < 10) LCD_I2C_Printf("%d:0%d",hora,min);
			else LCD_I2C_Printf("%d:%d",hora,min);
			OSTimeDlyHMSM(0u, 0u, 2u, 0u);
			i=4;
		}
		if (i==4){
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("INGRESE HORA");
			LCD_I2C_SetCursor(1, 0);
			LCD_I2C_Printf("DE ACTIVACION");
			OSTimeDlyHMSM(0u, 0u, 2u, 0u);
			i=5;
		}

		if (i==5){
			switch (indice){
			case 0:
				LCD_I2C_Printf("XX:XX");
				break;
			case 1:
				LCD_I2C_Printf("%dX:XX",hora_button[0]);
				break;
			case 2:
				LCD_I2C_Printf("%d%d:XX",hora_button[0],hora_button[1]);
				break;
			case 3:
				LCD_I2C_Printf("%d%d:%dX",hora_button[0],hora_button[1],minutos_button[0]);
				break;
			case 4:
				LCD_I2C_Printf("%d%d:%d%d",hora_button[0],hora_button[1],minutos_button[0],minutos_button[1]);
				break;
					}

			}
	}
	if (menu == 3){
		if (i==5){
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("HORA ACTIVACION");
			LCD_I2C_SetCursor(1, 0);
			if (minutos_activa < 10) LCD_I2C_Printf("%d:0%d",hora_activa,minutos_activa);
			else LCD_I2C_Printf("%d:%d",hora_activa,minutos_activa);
			OSTimeDlyHMSM(0u, 0u, 2u, 0u);
			i=0;
			}
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_Printf("HORA ACTUAL");
		LCD_I2C_SetCursor(1, 0);

		if ((seg < 10)&&( min < 10 )){
			LCD_I2C_Printf("%d:0%d:0%d",hora,min,seg);
		}
		else if (seg < 10){
			LCD_I2C_Printf("%d:%d:0%d",hora,min,seg);
		}
		else if (min < 10){
			LCD_I2C_Printf("%d:0%d:%d",hora,min,seg);
		}
		else LCD_I2C_Printf("%d:%d:%d",hora,min,seg);
	}

	if (menu == 4){
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_PutMeasure(pH, "pH");
		LCD_I2C_SetCursor(1,0);
		LCD_I2C_Printf("pH adc:%d",pH_adc);

	}

	if(menu == 5){
		LCD_I2C_PutMeasure(temperatura, "Temp");
	}

	if (menu == 6){
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_PutMeasure(nivel_clarif, "Niv Clar");
		LCD_I2C_SetCursor(1,0);
		LCD_I2C_PutMeasure(nivel_pH, "Niv pH-");
	}

	if (menu == 7){
		if (nivel_pileta == 0){
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("NIV PILETA: OK");
		}
		else {
			LCD_I2C_SetCursor(0,0);
			LCD_I2C_Printf("REVISAR");
			LCD_I2C_SetCursor(1,0);
			LCD_I2C_Printf("NIVEL PILETA");
		}

	}

	if (menu == 8){
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_Printf("Electrodos");
		LCD_I2C_SetCursor(1,0);
		LCD_I2C_Printf("0V");
	}

	if (menu == 9){
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_Printf("Electrodos");
		LCD_I2C_SetCursor(1,0);
		LCD_I2C_Printf("12V");
	}

	if (menu == 10){
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_Printf("Electrodos");
		LCD_I2C_SetCursor(1,0);
		LCD_I2C_Printf("9V");
	}

	if (menu == 11){
		LCD_I2C_SetCursor(0,0);
		LCD_I2C_Printf("Electrodos");
		LCD_I2C_SetCursor(1,0);
		LCD_I2C_Printf("6V");
	}


	OSTimeDlyHMSM(0u, 0u, 0u, 50u);

	LCD_I2C_Clear();
	}
}

static void Sensores_1 (void *p_arg){

(void)p_arg;
float temp;
while (DEF_TRUE){

	HAL_GPIO_WritePin(pH_encendido_GPIO_Port, pH_encendido_Pin, 1);

	//Obtengo nivel de la pileta
	nivel_pileta = HAL_GPIO_ReadPin(nivel_GPIO_Port, nivel_Pin) ;

	//Obtengo pH
	//pH = lectura_pH();

	//Obtengo temperatura
	temp = lectura_temp();

	if(temp < 50) temperatura = temp;

	OSTimeDlyHMSM(0u, 0u, 0u, 150u);
	}
}

static void Sensores_2 (void *p_arg){

(void)p_arg;
while (DEF_TRUE){

	//Obtengo Nivel Clarificador
	nivel_clarif = lectura_nivel_recip2();

	OSTimeDlyHMSM(0u, 0u, 1u, 0u);
	//Obtengo Nivel recipiente pH-
	nivel_pH = lectura_nivel_recip1();

	OSTimeDlyHMSM(0u, 0u, 1u, 0u);

	}
}

static void Led (void *p_arg){

(void)p_arg;
while (DEF_TRUE){

	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
	OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}

static void Bomba_filtrado (void *p_arg){

(void)p_arg;
while (DEF_TRUE){


	HAL_GPIO_WritePin(e_relay_GPIO_Port, e_relay_Pin, 0);
	OSTimeDlyHMSM(0u, 1u, 0u, 0u);
	//HAL_GPIO_WritePin(e_relay_GPIO_Port, e_relay_Pin, 1); // Descomentar despues
	OSTimeDlyHMSM(0u, 0u, 3u, 0u);
	}
}

static void Bomba_dosif_1 (void *p_arg){

(void)p_arg;
while (DEF_TRUE){

	HAL_GPIO_WritePin(dosif_clarif_GPIO_Port, dosif_clarif_Pin, 0);
	OSTimeDlyHMSM(0u, 0u, 5u, 0u);
	//HAL_GPIO_WritePin(dosif_clarif_GPIO_Port, dosif_clarif_Pin, 1);//Descomentar despues
	OSTimeDlyHMSM(0u, 0u, 2u, 0u);
	//OSTimeDlyHMSM(0u, 0u, 2u, 0u);

	}
}

static void Bomba_dosif_2 (void *p_arg){

(void)p_arg;
while (DEF_TRUE){

	HAL_GPIO_WritePin(dosif_pH_GPIO_Port, dosif_pH_Pin, 0);
	OSTimeDlyHMSM(0u, 0u, 5u, 0u);
	//HAL_GPIO_WritePin(dosif_pH_GPIO_Port, dosif_pH_Pin, 1); // DEscomentar despues
	OSTimeDlyHMSM(0u, 0u, 2u, 0u);
	//OSTimeDlyHMSM(0u, 0u, 2u, 0u);


	}
}

static void Ionizacion (void *p_arg){

(void)p_arg;
int sw_polar = 0; //Para cambiar polaridad electrodos

while (DEF_TRUE){


	if (menu == 8){	// Apagado DAC
		HAL_GPIO_WritePin(dac_on_GPIO_Port, dac_on_Pin, 0);
		HAL_GPIO_WritePin(dac_1_GPIO_Port, dac_1_Pin, 0);
		HAL_GPIO_WritePin(dac_2_GPIO_Port, dac_2_Pin, 0);
	}

	if (menu == 9){ //DAC ON SIN RESISTENCIAS EN PARALELO 12V SALIDA
		HAL_GPIO_WritePin(dac_on_GPIO_Port, dac_on_Pin, 1);
		HAL_GPIO_WritePin(dac_1_GPIO_Port, dac_1_Pin, 0);
		HAL_GPIO_WritePin(dac_2_GPIO_Port, dac_2_Pin, 0);
	}

	if (menu == 10){ //DAC CON UNA RESISTENCIA EN PARALELO 9V SALIDA
		HAL_GPIO_WritePin(dac_on_GPIO_Port, dac_on_Pin, 1);
		HAL_GPIO_WritePin(dac_1_GPIO_Port, dac_1_Pin, 1);
		HAL_GPIO_WritePin(dac_2_GPIO_Port, dac_2_Pin, 0);
	}

	if (menu == 11){ // DAC CON OTRA RESISTENCIA EN PARALELO 6V SALIDA
		HAL_GPIO_WritePin(dac_on_GPIO_Port, dac_on_Pin, 1);
		HAL_GPIO_WritePin(dac_1_GPIO_Port, dac_1_Pin, 0);
		HAL_GPIO_WritePin(dac_2_GPIO_Port, dac_2_Pin, 1);
	}

	if ((menu >= 9)&&(menu<=11)){ // Cada 600 son 5 minutos (Recomendado) pongo 125 asi toglea cada 1 minuto
		if (sw_polar < 600){
			HAL_GPIO_WritePin(puente_H_1_GPIO_Port, puente_H_1_Pin, 1);
			HAL_GPIO_WritePin(puente_H_2_GPIO_Port, puente_H_2_Pin, 0);
			sw_polar = sw_polar + 1;
		}
		else {
			if (sw_polar == 1200) sw_polar = 0;
			HAL_GPIO_WritePin(puente_H_1_GPIO_Port, puente_H_1_Pin, 0);
			HAL_GPIO_WritePin(puente_H_2_GPIO_Port, puente_H_2_Pin, 1);
			sw_polar = sw_polar + 1;
		}

		/*HAL_GPIO_WritePin(puente_H_1_GPIO_Port, puente_H_1_Pin, 1);
		OSTimeDlyHMSM(0u, 0u, 0u, 2u);
		HAL_GPIO_WritePin(puente_H_2_GPIO_Port, puente_H_2_Pin, 0);*/

	}
	else {
		HAL_GPIO_WritePin(puente_H_1_GPIO_Port, puente_H_1_Pin, 1);
		HAL_GPIO_WritePin(puente_H_2_GPIO_Port, puente_H_2_Pin, 0);
	}

	OSTimeDlyHMSM(0u, 0u, 0u, 500u);


	}
}

static void Menu (void *p_arg){

(void)p_arg;

int tecla_pres_prev=0;
int j=0;
CPU_BOOLEAN hora_check;
CPU_BOOLEAN hora_seteo_check=0;

while (DEF_TRUE){

	if (menu == 0){

		if((pres_tecla==1) && (tecla_pres_prev ==0) ){

			if ((tecla_pres < 10) || (tecla_pres==0)){
			litros[indice] = tecla_pres;
			indice = indice + 1;
				}

			if (tecla_pres == 10){
						litros_pileta=0;
						for (j=0;j<indice;j++){
								litros_pileta = litros_pileta + litros[j]*pow(10,((indice-1)-j)) ;
									}
						for (j=0;j<indice;j++){
							litros[j]=0;
						}
							indice=0;
							menu = 1;
					}

			if (tecla_pres==11){
					litros_pileta=0;
					for (j=0;j<indice;j++){
						litros[j]=0;
						}
					indice=0;
				}

			}

		}

	if (menu == 1){

		if((pres_tecla==1) && (tecla_pres_prev ==0)){

			if ((tecla_pres < 10) || (tecla_pres==0)){
				if ((indice==0)||(indice==1)){
					hora_button[indice] = tecla_pres;
					indice = indice + 1;
					}
				else if ((indice==2)||(indice==3)){
					minutos_button[indice-2] = tecla_pres;
					indice = indice + 1;
					if (indice == 4) hora_check = 1;
				}
			}

			else if ((tecla_pres == 10) && (hora_check==1)){
						hora = hora_button[0]*pow(10,1) + hora_button[1]*pow(10,0);
						min = minutos_button[0]*pow(10,1) + minutos_button[1]*pow(10,0);
						indice=0;
						menu = 2;
						hora_check = 0;
				}
			else if (tecla_pres==11){
					indice = 0;
					hora = 0;
					min = 0;
				}

				}

			}

	if (menu == 2){

		if((pres_tecla==1) && (tecla_pres_prev ==0)){

			if ((tecla_pres < 10) || (tecla_pres==0)){
				if ((indice==0)||(indice==1)){
					hora_button[indice] = tecla_pres;
					indice = indice + 1;
					}
				else if ((indice==2)||(indice==3)){
					minutos_button[indice-2] = tecla_pres;
					indice = indice + 1;
					if (indice == 4) hora_seteo_check = 1;
				}
			}

			else if ((tecla_pres == 10) && (hora_seteo_check==1)){
						hora_activa = hora_button[0]*pow(10,1) + hora_button[1]*pow(10,0);
						minutos_activa = minutos_button[0]*pow(10,1) + minutos_button[1]*pow(10,0);
						indice=0;
						menu = 3;
						hora_seteo_check=0;
				}
			else if (tecla_pres==11){
					indice=0;
					hora_activa = 0;
					minutos_activa = 0;
				}

				}

			}

	if (menu == 3){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){

			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 4;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	if (menu == 4){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 5;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}

	}

	if (menu == 5){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 6;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}

	}

	if (menu == 6){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 7;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	if (menu == 7){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 8;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	if (menu == 8){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 9;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	if (menu == 9){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 10;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	if (menu == 10){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 11;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	if (menu == 11){
		if((pres_tecla==1) && (tecla_pres_prev ==0)){
			if (tecla_pres == 10){
				indice = indice + 1;
				if (indice == 2){
					menu = 3;
					indice = 0;
				}

			}
			else if (tecla_pres == 13){
				menu = 0;
			}
		}
	}

	tecla_pres_prev = pres_tecla;
	OSTimeDlyHMSM(0u, 0u, 0u, 50u);

	}
}

static void Manejo_salidas (void *p_arg){

(void)p_arg;
while (DEF_TRUE){

	OSTimeDlyHMSM(0u, 0u, 0u, 50u);
	}
}

static void medicion_pH (void *p_arg){

(void)p_arg;
int promedio;
uint8_t k=0;
int pH_promedio=0;
//float pH_noTrunc;
while (DEF_TRUE){



	promedio = 0;

	for (k=0;k<100;k++){
		HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1, 1); //Otros ponen 1 en vez de HAL_MAX_DELAY... ver
		pH_promedio = HAL_ADC_GetValue(&hadc1);
		promedio = promedio + pH_promedio;
		OSTimeDlyHMSM(0u, 0u, 0u, 5u);

	}

	pH_adc = promedio* 0.01;
	pH = -5.7*(pH_adc*0.0008056) + 21.34 - 0.1;

	/*pH_noTrunc = -5.7*(pH_adc*0.0008056) + 21.34;

	pH = round(pH_noTrunc*10) *0.1;*/

	//HAL_ADC_Stop(&hadc1);

	OSTimeDlyHMSM(0u, 0u, 0u, 500u);

	}
}







/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
