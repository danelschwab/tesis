/*
*********************************************************************************************************
*                                            EXAMPLE CODE
*
*               This file is provided as an example on how to use Micrium products.
*
*               Please feel free to use any application code labeled as 'EXAMPLE CODE' in
*               your application products.  Example code may be used as is, in whole or in
*               part, or may be used as a reference only. This file can be modified as
*               required to meet the end-product requirements.
*
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*
*                    You can find our product's documentation at: doc.micrium.com
*
*                          For more information visit us at: www.micrium.com
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                      APPLICATION CONFIGURATION
*
*                                            EXAMPLE CODE
*
* Filename : app_cfg.h
*********************************************************************************************************
*/

#ifndef  _APP_CFG_H_
#define  _APP_CFG_H_


/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/

#include  <stdarg.h>
#include  <stdio.h>
#include  "cpu_cfg.h"                                           /* Needed for the Kernel&Non-Kernel aware Cortex-M port */


/*
*********************************************************************************************************
*                                       MODULE ENABLE / DISABLE
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                           TASK PRIORITIES
*********************************************************************************************************
*/

#define APP_CFG_STARTUP_TASK_PRIO           2u
#define APP_CFG_Tiempo_PRIO  				3u
#define APP_CFG_Teclas_PRIO  				4u
#define APP_CFG_Display_PRIO  				13u
#define APP_CFG_Sensores_1_PRIO  			6u
#define APP_CFG_Sensores_2_PRIO  			7u
#define APP_CFG_Led_PRIO  					8u
#define APP_CFG_Bomba_filtrado_PRIO 		9u
#define APP_CFG_Bomba_dosif_1_PRIO  		10u
#define APP_CFG_Bomba_dosif_2_PRIO  		11u
#define APP_CFG_Ionizacion_PRIO  			12u
#define APP_CFG_Menu_PRIO  					5u
#define APP_CFG_Manejo_salidas_PRIO  		14u
#define APP_CFG_medicion_pH_PRIO  		    15u

#define  OS_TASK_TMR_PRIO                  (OS_LOWEST_PRIO - 2u)


/*
*********************************************************************************************************
*                                          TASK STACK SIZES
*                             Size of the task stacks (# of OS_STK entries)
*********************************************************************************************************
*/

#define APP_CFG_STARTUP_TASK_STK_SIZE    128u
#define APP_CFG_Tiempo_STK_SIZE  		 128u
#define APP_CFG_Teclas_STK_SIZE  		 128u
#define APP_CFG_Display_STK_SIZE  		 300u
#define APP_CFG_Sensores_1_STK_SIZE  	 128u
#define APP_CFG_Sensores_2_STK_SIZE  	 200u
#define APP_CFG_Led_STK_SIZE  			 128u
#define APP_CFG_Bomba_filtrado_STK_SIZE  128u
#define APP_CFG_Bomba_dosif_1_STK_SIZE   128u
#define APP_CFG_Bomba_dosif_2_STK_SIZE   128u
#define APP_CFG_Ionizacion_STK_SIZE  	 128u
#define APP_CFG_Menu_STK_SIZE  			 128u
#define APP_CFG_Manejo_salidas_STK_SIZE  128u
#define APP_CFG_medicion_pH_STK_SIZE     128u


/*
*********************************************************************************************************
*                                     TRACE / DEBUG CONFIGURATION
*********************************************************************************************************
*/

#ifndef  TRACE_LEVEL_OFF
#define  TRACE_LEVEL_OFF                    0u
#endif

#ifndef  TRACE_LEVEL_INFO
#define  TRACE_LEVEL_INFO                   1u
#endif

#ifndef  TRACE_LEVEL_DBG
#define  TRACE_LEVEL_DBG                    2u
#endif

#define  APP_TRACE_LEVEL                   TRACE_LEVEL_OFF
#define  APP_TRACE                         printf

#define  APP_TRACE_INFO(x)    ((APP_TRACE_LEVEL >= TRACE_LEVEL_INFO)  ? (void)(APP_TRACE x) : (void)0)
#define  APP_TRACE_DBG(x)     ((APP_TRACE_LEVEL >= TRACE_LEVEL_DBG)   ? (void)(APP_TRACE x) : (void)0)


/*
*********************************************************************************************************
*                                             MODULE END
*********************************************************************************************************
*/

#endif                                                          /* End of module include.              */
