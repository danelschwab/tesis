/*
 ************************************************************************************************
 *
 *
 *
 * Filename		 : lcd_i2c.h
 * Programmer(s) :
 * Created on	 : 18 may. 2021
 * Description 	 : header file of lcd_i2c.c
 ************************************************************************************************
 */

#ifndef INC_LCD_I2C_H_
#define INC_LCD_I2C_H_

/*
 ************************************************************************************************
 *								 	INCLUDE FILES
 ************************************************************************************************
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "stm32f1xx_hal.h"
#include <ucos_ii.h>

/*
 ************************************************************************************************
 *									CONSTANTS & MACROS
 ************************************************************************************************
 */


/*
 ************************************************************************************************
 *									FUNCTION PROTOTYPES
 ************************************************************************************************
 */

bool LCD_I2C_Init				(I2C_HandleTypeDef *pI2cHandle);	// ok
void LCD_I2C_SetCursor			(uint8_t row, uint8_t col);			// ok
void LCD_I2C_GoFirstLine		(void);								// ok
void LCD_I2C_GoSecondLine		(void);								// ok
void LCD_I2C_TwoLines			(void);								// ok
void LCD_I2C_OneLine			(void);								// not ok
void LCD_I2C_CursorShow			(bool state);						// ok
void LCD_I2C_Clear				(void);								// ok
void LCD_I2C_Printf				(const char *str, ...);				// ok
void LCD_I2C_Display			(bool state);						// ok
void LCD_I2C_ShiftRight			(uint8_t offset);					// ok
void LCD_I2C_ShiftLeft			(uint8_t offset);					// ok
char * float_to_char(float x, char *p, int CHAR_BUFF_SIZE);
void LCD_I2C_PutMeasure         (float x, char *p);


#endif /* INC_LCD_I2C_H_ */
